var express = require('express');
var router = express.Router();

var BonusImageUpload = require("../utility/BonusImageUpload");


const {
    bonusTestingRoute,
    addBonusRoute,
    fetchBonusesRoute,
    editBonusRoute,
    findOneBonusRoute,
    deleteBonusRoute
} = require("../controller/BonusController");



/**
* @route Get /
* @desc  Bonuse  Testing Route
* @access Public
**/
router.get('/', bonusTestingRoute);

/**
* @route Get /bonus/addBonus
* @desc  admin addBonus Route
* @access Private
**/
router.post('/addBonus',
    BonusImageUpload.fields([{ name: "Image", maxCount: 1 }]),
    addBonusRoute
);

/**
* @route Get /bonus/fetchBonus
* @desc  admin fetchBonus Route
* @access Public
**/
router.get('/fetchBonus', fetchBonusesRoute);

/**
* @route Get /bonus/editBonus/:id
* @desc  admin editBonus Route
* @access Private
**/

router.post('/editBonus/:id',
    BonusImageUpload.fields([{ name: "Image", maxCount: 1 }]),
    editBonusRoute
);

/**
* @route Get /bonus/findOneBonus/:id
* @desc  admin findOneBonus Route
* @access Public
**/
router.get('/findOneBonus/:id', findOneBonusRoute);

/** 
* @route Delete /deleteBonus/:id
* @desc  delete Bonus Route
* @access Private
**/
router.delete('/deleteBonus/:id', deleteBonusRoute);



module.exports = router;
