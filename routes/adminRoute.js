var express = require('express');
var router = express.Router();

const {
    adminTestingRoute,
    adminRegisterRoute,
    adminLoginRoute,
} = require("../controller/adminController");

/**
* @route Get /admin/
* @desc  admin Page Testing Route
* @access Public
**/
router.get('/', adminTestingRoute);

/**
* @route Get /admin/register
* @desc  admin register Route
* @access Public
**/
router.post('/register', adminRegisterRoute);

/**
* @route Get /admin/login
* @desc  admin Login Route
* @access Public
**/
router.post('/login', adminLoginRoute);

module.exports = router;
