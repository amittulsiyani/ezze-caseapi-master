var express = require('express');
var router = express.Router();


const {
    userTestingRoute,
    userRegisterRoute,
    userLoginRoute,
    userForgotPasswordRoute,
    userForgotPasswordOTPVerifyRoute,
    userResetPasswordRoute,
    fetchUsersRoute,
    addCoinsInUser,
    findOneUserRoute,
    addRedeemInUser
} = require("../controller/userController");

/**
* @route Get /user/
* @desc  user Page Testing Route
* @access Public
**/
router.get('/', userTestingRoute);

/**
* @route Post /user/register
* @desc  user register Route
* @access Public
**/
router.post('/register', userRegisterRoute);

/**
* @route Post /user/login
* @desc  user Login Route
* @access Public
**/
router.post('/login', userLoginRoute);

/**
* @route Post /user/forgotPassword
* @desc  user forgotPassword Route
* @access Public
**/
router.post('/forgotPassword', userForgotPasswordRoute);

/**
* @route Post /user/forgotPasswordOTPVerify
* @desc  user forgotPasswordOTPVerify Route
* @access Public
**/
router.post('/forgotPasswordOTPVerify', userForgotPasswordOTPVerifyRoute);

/**
* @route Post /user/resetPassword
* @desc  user resetPassword Route
* @access Public
**/
router.post('/resetPassword', userResetPasswordRoute);


/**
* @route Get /user/fetchUsers
* @desc  user fetchUsers Route
* @access Public
**/
router.get('/fetchUsers', fetchUsersRoute);

/**
* @route Get /user/findOneUser/:id
* @desc  user findOneUser Route
* @access Public
**/
router.get('/findOneUser/:id', findOneUserRoute);


/**
* @route  /user/addCoins
* @desc  user /addCoins Route
* @access Public
**/
router.post('/addCoins', addCoinsInUser);

/**
* @route  /user/addRedeemInUser
* @desc  user /addRedeemInUser Route
* @access Public
**/
router.post('/addRedeemInUser', addRedeemInUser);

module.exports = router;
