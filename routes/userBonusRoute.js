var express = require('express');
var router = express.Router();

const {
    userBonusTestingRoute,
    userBonusActiveRoute,
    fetchUsersActivatedBonusesRoute,
    fetchUserActivatedBonusByIDRoute
} = require("../controller/userBonusController");



/**
* @route Get /
* @desc  userBonus Testing Route
* @access Public
**/

router.get('/', userBonusTestingRoute);

/**
* @route Post /userBonusActive
* @desc  userBonusActive  Route
* @access Public
**/

router.post('/userBonusActive', userBonusActiveRoute);

/**
* @route Get /fetchUsersActivatedBonusesRoute
* @desc  fetchUsersActivatedBonusesRoute Route
* @access Public
**/

router.get('/fetchUsersActivatedBonuses', fetchUsersActivatedBonusesRoute);

/**
* @route Get /fetchUserActivatedBonusByID/:id
* @desc  fetchUserActivatedBonusByID Route
* @access Public
**/

router.get('/fetchUserActivatedBonusByID/:id', fetchUserActivatedBonusByIDRoute);


module.exports = router;
