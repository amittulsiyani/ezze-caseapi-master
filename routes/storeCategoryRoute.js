var express = require('express');
var router = express.Router();

var StoreImageUpload = require("../utility/StoreImageUpload");

const {
    storeTestingRoute,
    addCategoryRoute,
    fetchCategoryRoute,
    editCategoryRoute,
    findOneCategoryRoute,
    deleteCategoryRoute,
    addSubCategoryRoute,
    fetchSubCategoryRoute,
    editSubCategoryRoute,
    findOneSubCategoryRoute,
    deleteSubCategoryRoute,
    fetchSubCategoryByCategoryIDRoute
} = require("../controller/StoreCategoryController");

/**
* @route Get /store
* @desc  store Testing Route
* @access Public
**/
router.get('/', storeTestingRoute);

/**
* @route Get /store/addCategory
* @desc  store addCategory Route
* @access Private
**/
router.post('/addCategory',
    StoreImageUpload.fields([{ name: "Image", maxCount: 1 }]),
    addCategoryRoute
);

/**
* @route Get /store/fetchCategory
* @desc  store fetchCategory Route
* @access Public
**/
router.get('/fetchCategory', fetchCategoryRoute);

/**
* @route Get /store/editCategory/:id
* @desc  store editCategory Route
* @access Private
**/

router.post('/editCategory/:id',
    StoreImageUpload.fields([{ name: "Image", maxCount: 1 }]),
    editCategoryRoute
);

/**
* @route Get /store/findOneCategory/:id
* @desc  store findOneCategory Route
* @access Public
**/
router.get('/findOneCategory/:id', findOneCategoryRoute);

/** 
* @route Delete /deleteCategory/:id
* @desc  store delete Category Route
* @access Private
**/
router.delete('/deleteCategory/:id', deleteCategoryRoute);

/**
* @route Get /store/addSubCategory
* @desc  store addSubCategory Route
* @access Private
**/
router.post('/addSubCategory',
    StoreImageUpload.fields([{ name: "Image", maxCount: 1 }]),
    addSubCategoryRoute
);

/**
* @route Get /store/fetchSubCategory
* @desc  store fetchSubCategory Route
* @access Public
**/
router.get('/fetchSubCategory', fetchSubCategoryRoute);

/**
* @route Get /store/fetchSubCategoryByCategoryID/:CategoryID
* @desc  store editSubCategory Route
* @access Private
**/

router.get('/fetchSubCategoryByCategoryID/:CategoryID', fetchSubCategoryByCategoryIDRoute);

/**
* @route Get /store/editSubCategory/:id
* @desc  store editSubCategory Route
* @access Private
**/

router.post('/editSubCategory/:id',
    StoreImageUpload.fields([{ name: "Image", maxCount: 1 }]),
    editSubCategoryRoute
);

/**
* @route Get /store/findOneSubCategory/:id
* @desc  store findOneSubCategory Route
* @access Public
**/
router.get('/findOneSubCategory/:id', findOneSubCategoryRoute);

/** 
* @route Delete /deleteSubCategory/:id
* @desc  store delete SubCategory Route
* @access Private
**/
router.delete('/deleteSubCategory/:id', deleteSubCategoryRoute);



module.exports = router;
