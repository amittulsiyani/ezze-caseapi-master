var express = require('express');
var router = express.Router();

const {
  indexTestingRoute
} = require("../controller/indexController");

/**
* @route Get /
* @desc  index Page Testing Route
* @access Public
**/
router.get('/', indexTestingRoute);


module.exports = router;
