var JWT = require("jsonwebtoken");
const { secretKey } = require("../keys/keys").url;

exports.isLoggedIn = (req, res, next) => {
    const token = req.header("auth-token");
    if (!token) return res.json({ message: "Access Denied", status: 0 });
    try {
        const verified = JWT.verify(token, secretKey);
        req.user = verified.user;
        next();
    }
    catch (error) {
        let message;
        if (!req.user) message = "Session Timeout! User Not Found";
        else message = error;
        res.json({ message: message, status: 0 })
    }
}