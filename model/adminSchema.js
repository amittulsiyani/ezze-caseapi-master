var mongoose = require("mongoose");

var adminSchema = mongoose.Schema({
    email: {
        type: String,
    },
    password: {
        type: String,
    },
    passwordOTP: {
        type: String,
        default: null,
    },
}, { timestamps: true })

module.exports = mongoose.model("admin", adminSchema);