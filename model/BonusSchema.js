var mongoose = require("mongoose");

var BonusSchema = mongoose.Schema({
    Title: {
        type: String
    },

    Description: {
        type: String
    },

    Image: {
        filedname: String,
        originalname: String,
        mimetype: String,
        filename: String,
        path: String,
        size: String
    },

    Type: {
        type: String
    },

}, { timestamps: true })

module.exports = mongoose.model("Bonus", BonusSchema);