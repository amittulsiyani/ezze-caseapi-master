var mongoose = require("mongoose");

var userSchema = mongoose.Schema({
    username: {
        type: String,
    },
    email: {
        type: String,
    },
    password: {
        type: String,
    },
    passwordOTP: {
        type: String,
        default: null,
    },
    myEarnings: {
        type: Number,
        default: 0
    },
    loginType: {
        type: String,
        default: "manually"
    },
    authID: {
        type: String,
        default: null,
    },
    userRedeemHistory: [],
    userActivatedBonusesHistory: [],

}, { timestamps: true })

module.exports = mongoose.model("user", userSchema);