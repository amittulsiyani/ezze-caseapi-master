var mongoose = require("mongoose");

var userBonusSchema = mongoose.Schema({

    userID: { type: mongoose.Schema.Types.ObjectId, ref: 'user' },
    BonusID: { type: mongoose.Schema.Types.ObjectId, ref: 'Bonus' },
    Active: {
        type: Boolean,
        default: false
    },
    time: {
        type: Date,
        default: Date.now
    },
    expireTime: {
        type: Date,
        default: Date.now
    }

}, { timestamps: true })

module.exports = mongoose.model("userBonus", userBonusSchema);