var mongoose = require("mongoose");

var StoreSubCategorySchema = mongoose.Schema({
    CategoryID: {
        type: String
    },

    Title: {
        type: String
    },

    Description: {
        type: String
    },

    Image: {
        filedname: String,
        originalname: String,
        mimetype: String,
        filename: String,
        path: String,
        size: String
    },

    Type: {
        type: String
    },

    coins: {
        type: Number
    },

}, { timestamps: true })

module.exports = mongoose.model("StoreSubCategory", StoreSubCategorySchema);