var bcrypt = require("bcryptjs");
var { secretKey } = require("../keys/keys").url;
var adminSchema = require("../model/adminSchema");
var JWT = require("jsonwebtoken");

/**
* @route Get /admin/
* @desc  admin Testing Route
**/

exports.adminTestingRoute = (req, res, next) => {
    res.json({ message: "admin Testing Route" })
}

/**
* @route Post /admin/register
* @desc  admin register Route
**/

exports.adminRegisterRoute = (req, res, next) => {

    const { email, password } = req.body;

    if (!email) {
        return res.json({ message: "email must be required.", status: 0 });
    }

    if (!password) {
        return res.json({ message: "password must be required.", status: 0 });
    }

    const newAdmin = new adminSchema({ email, password, });

    if (email && password) {

        adminSchema.findOne({ email })
            .then((admin) => {
                if (admin)
                    return res.json({ message: "email already exists.", status: 0 });
                bcrypt.genSalt(10, (err, salt) => {
                    bcrypt.hash(newAdmin.password, salt, (err, hash) => {
                        if (err) throw err;
                        newAdmin.password = hash;
                        newAdmin
                            .save()
                            .then((admin) => {
                                res.json({ message: "admin registered successfully.", status: 1, admin });
                            })
                            .catch(() =>
                                res.json({ message: "Internal Server Error!.", status: 0 })
                            );
                    });
                });
            })
            .catch(() =>
                res.json({ message: "Internal Server Error!.", status: 0 })
            );
    }
};



/**
* @route Post /admin/login
* @desc  admin login Route
**/

exports.adminLoginRoute = (req, res, next) => {

    const { email, password } = req.body

    if (!email) {
        return res.json({ message: "email must be required.", status: 0 });
    }

    if (!password) {
        return res.json({ message: "password must be required.", status: 0 });
    }

    if (email && password) {
        adminSchema.findOne({ email })
            .then((admin) => {
                if (!admin)
                    return res.json({ message: "email not found.", status: 0, });
                bcrypt.compare(password, admin.password)
                    .then((isMatch) => {
                        if (!isMatch) return res.json({ message: "password not match", status: 0 });
                        const token = JWT.sign({ admin }, secretKey, {
                            expiresIn: 360000,
                        });
                        req.header("auth-token", token);
                        res.json({ message: "admin login Successfully.", status: 1, admin, token });
                    })
                    .catch(() =>
                        res.json({ message: "Internal Server Error1!", status: 0 })
                    );
            })
            .catch(() =>
                res.json({ message: "Internal Server Error2!", status: 0 })
            );
    }
};




