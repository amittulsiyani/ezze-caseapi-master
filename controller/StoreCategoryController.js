
var StoreCategorySchema = require("../model/StoreCategorySchema");
var StoreSubCategorySchema = require("../model/StoreSubCategorySchema");

/**
* @route Get /store
* @desc  bonus Testing Route
**/

exports.storeTestingRoute = (req, res, next) => {
    res.json({ message: "store Testing Route" })
}

/**
* @route Post /store/addCategory
* @desc  store addCategory Route
**/

exports.addCategoryRoute = (req, res, next) => {

    const Image = req.files.Image[0];
    const { Title, Type, Description } = req.body;

    if (!Title) {
        return res.json({ message: "Title must be required.", status: 0 });
    }

    if (!Type) {
        return res.json({ message: "Type must be required.", status: 0 });
    }

    if (!Description) {
        return res.json({ message: "Description must be required.", status: 0 });
    }

    if (!Image) {
        return res.json({ message: "Image must be required.", status: 0 });
    }

    if (Title && Type && Description && Image) {
        const newCategory = new StoreCategorySchema({ Title, Type, Description, Image });
        StoreCategorySchema.findOne({ Title })
            .then((Title) => {
                if (Title) return res.json({ message: "Title already exists.", status: 0 });
                newCategory.save()
                    .then(() => {
                        res.json({ message: "Category add successfully.", status: 1, newCategory });
                    })
                    .catch((err) => {
                        console.log(err)
                        res.json({ message: "Internal Server Error!.", status: 0 })
                    });
            })
            .catch(() => {
                res.json({ message: "Internal server Error!", status: 0 })
            })
    }

};


/**
* @route Get /store/fetchCategory
* @desc  store fetchCategory Route
**/

exports.fetchCategoryRoute = (req, res, next) => {
    StoreCategorySchema.find()
        .then(Categories => {
            res.json({ message: "Categories Lists", status: 1, Categories })
        })
        .catch(() => res.json({ message: "internal server Error!", status: 0 }))
};

/**
* @route Get /store/editCategory/:id
* @desc  store editCategory Route
**/
exports.editCategoryRoute = (req, res, next) => {
    const id = req.params.id;
    const { Title, Type, Description } = req.body;
    const data = { Title, Type, Description }
    if (req.files.Image) {
        data["Image"] = req.files.Image[0];
    }

    if (!id) {
        return res.json({ message: "CategoryID must be required.", status: 0 });
    }

    var editCategory = (data);

    StoreCategorySchema.findById(id)
        .then((id) => {
            if (!id) return res.json({ message: "Category not found!", status: 0 });
            StoreCategorySchema.findByIdAndUpdate(id, { $set: editCategory }, { new: true, useFindAndModify: false })
                .then(() => {
                    res.json({ message: "Category updated successfully.", status: 1 })
                })
                .catch(() => res.json({ message: "internal server Error!", status: 0 }))
        })
        .catch(() => res.json({ message: "internal server Error!", status: 0 }))
};

/**
* @route Get /store/findOneCategory/:id
* @desc  store findOneCategory Route
**/
exports.findOneCategoryRoute = (req, res, next) => {

    const id = req.params.id;
    if (!id) {
        return res.json({ message: "Category must be required.", status: 0 });
    }
    StoreCategorySchema.findById(id)
        .then((id) => {
            if (!id) return res.json({ message: "Category not found!", status: 0 });
            StoreCategorySchema.findById(id)
                .then((data) => res.json({ message: "Find One Category", status: 1, data }))
                .catch(() => res.json({ message: "internal server Error1!", status: 0 }))
        })
        .catch(() => res.json({ message: "internal server Error2!", status: 0 }))
};

/**
* @route Get /store/deleteCategory/:id
* @desc  store deleteCategory Route
**/
exports.deleteCategoryRoute = (req, res, next) => {
    const id = req.params.id;
    if (!id) {
        return res.json({ message: "CategoryID must be required.", status: 0 });
    }
    StoreCategorySchema.findById(id)
        .then((id) => {
            if (!id) return res.json({ message: "Category not found!", status: 0 });
            StoreCategorySchema.findByIdAndDelete(id)
                .then(() => res.json({ message: "Category Deleted Successfully", status: 1 }))
                .catch(() => res.json({ message: "internal server Error!", status: 0 }))
        })
        .catch(() => res.json({ message: "internal server Error!", status: 0 }))
};


/**
* @route Post /store/addSubCategory
* @desc  store addSubCategory Route
**/

exports.addSubCategoryRoute = (req, res, next) => {
    const Image = req.files.Image[0];
    const { Title, Type, Description, coins, CategoryID } = req.body;

    console.log(Title, Type, Description, coins, CategoryID)

    if (!Title) {
        return res.json({ message: "Title must be required.", status: 0 });
    }

    if (!Type) {
        return res.json({ message: "Type must be required.", status: 0 });
    }

    if (!Description) {
        return res.json({ message: "Description must be required.", status: 0 });
    }

    if (!Image) {
        return res.json({ message: "Image must be required.", status: 0 });
    }

    if (!coins) {
        return res.json({ message: "coins must be required.", status: 0 });
    }

    if (!CategoryID) {
        return res.json({ message: "categoryID must be required.", status: 0 });
    }

    if (Title && Type && Description && Image && CategoryID && coins) {
        const newSubCategory = new StoreSubCategorySchema({ Title, Type, Description, Image, CategoryID, coins });
        StoreSubCategorySchema.findOne({ Title })
            .then((Title) => {
                if (Title) return res.json({ message: "Title already exists.", status: 0 });
                newSubCategory.save()
                    .then(() => {
                        res.json({ message: "Sub-Category add successfully.", status: 1, newSubCategory });
                    })
                    .catch((err) => {
                        console.log(err)
                        res.json({ message: "Internal Server Error!.", status: 0 })
                    });
            })
            .catch(() => {
                res.json({ message: "Internal server Error!", status: 0 })
            })
    }

};


/**
* @route Get /store/fetchSubCategory
* @desc  store fetchSubCategory Route
**/

exports.fetchSubCategoryRoute = (req, res, next) => {
    StoreSubCategorySchema.find()
        .then(SubCategories => {
            res.json({ message: "Sub-Categories Lists", status: 1, SubCategories })
        })
        .catch(() => res.json({ message: "internal server Error!", status: 0 }))
};

/**
* @route Get /store/fetchSubCategoryByCategoryID/:CategoryID
* @desc  store fetchSubCategoryByCategoryID Route
**/

exports.fetchSubCategoryByCategoryIDRoute = (req, res, next) => {

    const CategoryID = req.params.CategoryID;

    if (!CategoryID) {
        return res.json({ message: "CategoryID must be required.", status: 0 });
    }

    StoreSubCategorySchema.find({ CategoryID })
        .then(SubCategories => {
            res.json({ message: "Sub-Categories Lists By CategoryId", status: 1, SubCategories })
        })
        .catch(() => res.json({ message: "internal server Error!", status: 0 }))
};

/**
* @route Get /store/editSubCategory/:id
* @desc  store editSubCategory Route
**/
exports.editSubCategoryRoute = (req, res, next) => {
    const id = req.params.id;
    const { Title, Type, Description, coins, CategoryID } = req.body;
    const data = { Title, Type, Description, coins, CategoryID }
    if (req.files.Image) {
        data["Image"] = req.files.Image[0];
    }

    if (!id) {
        return res.json({ message: "Sub-CategoryID must be required.", status: 0 });
    }

    var editSubCategory = (data);


    StoreSubCategorySchema.findById(id)
        .then((id) => {
            console.log(id)
            if (!id) return res.json({ message: "Sub-Category not found!", status: 0 });
            StoreSubCategorySchema.findByIdAndUpdate(id, { $set: editSubCategory }, { new: true, useFindAndModify: false })
                .then(() => {
                    res.json({ message: "Sub-Category updated successfully.", status: 1 })
                })
                .catch(() => res.json({ message: "internal server Error2!", status: 0 }))
        })
        .catch(() => res.json({ message: "internal server Error1!", status: 0 }))
};

/**
* @route Get /store/findOneSubCategory/:id
* @desc  store findOneSubCategory Route
**/
exports.findOneSubCategoryRoute = (req, res, next) => {

    const id = req.params.id;
    if (!id) {
        return res.json({ message: "Sub-Category must be required.", status: 0 });
    }
    StoreSubCategorySchema.findById(id)
        .then((id) => {
            if (!id) return res.json({ message: "Sub-Category not found!", status: 0 });
            StoreSubCategorySchema.findById(id)
                .then((data) => res.json({ message: "Find One Sub-Category", status: 1, data }))
                .catch(() => res.json({ message: "internal server Error1!", status: 0 }))
        })
        .catch(() => res.json({ message: "internal server Error2!", status: 0 }))
};

/**
* @route Get /store/deleteSubCategory/:id
* @desc  store deleteSubCategory Route
**/
exports.deleteSubCategoryRoute = (req, res, next) => {
    const id = req.params.id;
    if (!id) {
        return res.json({ message: "Sub-CategoryID must be required.", status: 0 });
    }
    StoreSubCategorySchema.findById(id)
        .then((id) => {
            if (!id) return res.json({ message: "Sub-Category not found!", status: 0 });
            StoreSubCategorySchema.findByIdAndDelete(id)
                .then(() => res.json({ message: "Sub-Category Deleted Successfully", status: 1 }))
                .catch(() => res.json({ message: "internal server Error!", status: 0 }))
        })
        .catch(() => res.json({ message: "internal server Error!", status: 0 }))
};




