
var BonusSchema = require("../model/BonusSchema");

/**
* @route Get /bonus
* @desc  bonus Testing Route
**/

exports.bonusTestingRoute = (req, res, next) => {
    res.json({ message: "bonus Testing Route" })
}

/**
* @route Post /bonus/addBonus
* @desc  admin addBonus Route
**/

exports.addBonusRoute = (req, res, next) => {

    const Image = req.files.Image[0];
    const { Title, Type, Description } = req.body;

    if (!Title) {
        return res.json({ message: "Title must be required.", status: 0 });
    }

    if (!Type) {
        return res.json({ message: "Type must be required.", status: 0 });
    }

    if (!Description) {
        return res.json({ message: "Description must be required.", status: 0 });
    }

    if (!Image) {
        return res.json({ message: "Image must be required.", status: 0 });
    }

    if (Title && Type && Description && Image) {
        const newBonus = new BonusSchema({ Title, Type, Description, Image });
        BonusSchema.findOne({ Title })
            .then((Title) => {
                if (Title) return res.json({ message: "Title already exists.", status: 0 });
                newBonus.save()
                    .then(() => {
                        res.json({ message: "Bonus add successfully.", status: 1, newBonus });
                    })
                    .catch((err) => {
                        console.log(err)
                        res.json({ message: "Internal Server Error!.", status: 0 })
                    });
            })
            .catch(() => {
                res.json({ message: "Internal server Error!", status: 0 })
            })
    }

};


/**
* @route Get /bonus/fetchBonuses
* @desc  admin fetchBonus Route
**/

exports.fetchBonusesRoute = (req, res, next) => {
    BonusSchema.find()
        .then(Bonuses => {
            res.json({ message: "Bonus Lists", status: 1, Bonuses })
        })
        .catch(() => res.json({ message: "internal server Error!", status: 0 }))
};

/**
* @route Get /bonus/editBonus/:id
* @desc  admin editBonus Route
**/
exports.editBonusRoute = (req, res, next) => {
    const id = req.params.id;
    const { Title, Type, Description } = req.body;
    const data = { Title, Type, Description }
    if (req.files.Image) {
        data["Image"] = req.files.Image[0];
    }

    if (!id) {
        return res.json({ message: "BonusID must be required.", status: 0 });
    }

    var editBonus = (data);

    BonusSchema.findById(id)
        .then((id) => {
            if (!id) return res.json({ message: "Bonus not found!", status: 0 });
            BonusSchema.findByIdAndUpdate(id, { $set: editBonus }, { new: true, useFindAndModify: false })
                .then(() => {
                    res.json({ message: " Bonus updated successfully.", status: 1 })
                })
                .catch(() => res.json({ message: "internal server Error!", status: 0 }))
        })
        .catch(() => res.json({ message: "internal server Error!", status: 0 }))
};

/**
* @route Get /bonus/findOneBonus/:id
* @desc  admin findOneBonus Route
**/
exports.findOneBonusRoute = (req, res, next) => {

    const id = req.params.id;
    if (!id) {
        return res.json({ message: "BonusID must be required.", status: 0 });
    }
    console.log(id)
    BonusSchema.findById(id)
        .then((id) => {
            if (!id) return res.json({ message: "Bonus not found!", status: 0 });
            BonusSchema.findById(id)
                .then((data) => res.json({ message: "Find One Bonus ", status: 1, data }))
                .catch(() => res.json({ message: "internal server Error1!", status: 0 }))
        })
        .catch(() => res.json({ message: "internal server Error2!", status: 0 }))
};

/**
* @route Get /bonus/deleteBonus/:id
* @desc  admin deleteBonus Route
**/
exports.deleteBonusRoute = (req, res, next) => {
    const id = req.params.id;
    if (!id) {
        return res.json({ message: "BonusID must be required.", status: 0 });
    }
    BonusSchema.findById(id)
        .then((id) => {
            if (!id) return res.json({ message: "Bonus not found!", status: 0 });
            BonusSchema.findByIdAndDelete(id)
                .then(() => res.json({ message: "Bonus Deleted Successfully", status: 1 }))
                .catch(() => res.json({ message: "internal server Error!", status: 0 }))
        })
        .catch(() => res.json({ message: "internal server Error!", status: 0 }))
};