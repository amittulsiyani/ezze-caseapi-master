var bcrypt = require("bcryptjs");
var { adminEmail, adminPassword } = require("../keys/keys").url;
var userSchema = require("../model/userSchema");
var StoreSubCategorySchema = require("../model/StoreSubCategorySchema");
const nodemailer = require("nodemailer");


/**
* @route Get /user/
* @desc  user Testing Route
**/

exports.userTestingRoute = (req, res, next) => {
    res.json({ message: "user Testing Route" })
}

/**
* @route Post /user/register
* @desc  user register Route
**/

exports.userRegisterRoute = (req, res, next) => {

    const { username, email, password } = req.body;

    if (!username) {
        return res.json({ message: "username must be required.", status: 0 });
    }

    if (!email) {
        return res.json({ message: "email must be required.", status: 0 });
    }

    if (!password) {
        return res.json({ message: "password must be required.", status: 0 });
    }

    const newUser = new userSchema({ username, email, password, });

    if (username && email && password) {

        userSchema.findOne({ email })
            .then((user) => {
                if (user)
                    return res.json({ message: "email already exists.", status: 0 });
                bcrypt.genSalt(10, (err, salt) => {
                    bcrypt.hash(newUser.password, salt, (err, hash) => {
                        if (err) throw err;
                        newUser.password = hash;
                        newUser
                            .save()
                            .then((user) => {
                                res.status(201).json({ message: "user registered successfully.", status: 1, user });
                            })
                            .catch(() =>
                                res.json({ message: "Internal Server Error!.", status: 0 })
                            );
                    });
                });
            })
            .catch(() =>
                res.json({ message: "Internal Server Error!.", status: 0 })
            );
    }
};



/**
* @route Post /user/login
* @desc  user login Route
**/

exports.userLoginRoute = (req, res, next) => {

    const { email, password, loginType, authID } = req.body;



    if (!loginType) {
        return res.json({ message: "Login Type must be required.", status: 0 });
    }


    else if (loginType === "manually") {

        if (!email) {
            return res.json({ message: "email must be required.", status: 0 });
        }

        if (!password) {
            return res.json({ message: "password must be required.", status: 0 });
        }

        if (email && password) {


            userSchema.findOne({ email })
                .then((user) => {
                    if (!user)
                        return res
                            .json({ message: "email not found.", status: 0 });
                    bcrypt
                        .compare(password, user.password)
                        .then((isMatch) => {
                            if (!isMatch)
                                return res
                                    .json({ message: "password not match", status: 0 });
                            res.json({ message: "user login Successfully.", status: 1, user });
                        })
                        .catch(() =>
                            res.json({ message: "Internal Server Error!", status: 0 })
                        );
                })
                .catch(() =>
                    res.json({ message: "Internal Server Error!", status: 0 })
                );
        }
    }


    else if (loginType === "facebook" || loginType === "google") {

        if (!authID) {
            return res.json({ message: "authID must be required.", status: 0 });
        }
        else {

            const newUser = new userSchema({ authID, loginType });
            userSchema.findOne({ authID })
                .then((user) => {
                    if (!user) {
                        newUser
                            .save()
                            .then((user) => {
                                res.json({ message: "user loggedin successfully.", status: 1, user });
                            })
                            .catch(() =>
                                res.json({ message: "Internal Server Error!.", status: 0 })
                            );
                    }
                    else {

                        res.json({ message: "user loggedin successfully.", status: 1, user });

                    }
                })
                .catch(() => {
                    return res.json({ message: "Internal Server Error!", status: 0 });

                })
        }
    }
    else {

        return res.json({ message: "Login type is not Valid!", status: 0 });


    }
};


/**
* @route Post /user/forgotPassword
* @desc  user forgotPassword Route
**/
exports.userForgotPasswordRoute = (req, res, next) => {

    const { email } = req.body;


    let OTP = String(
        Math.floor(Math.random() * (999999 - 10000) + 10000)
    );

    if (!email) {
        return res.json({ message: "email must be required.", status: 0 });
    }



    if (email) {

        userSchema.findOne({ email })
            .then((user) => {
                if (!user) return res.json({ message: "email not found.", status: 0 });
                bcrypt.genSalt(10, (err, salt) => {
                    bcrypt.hash(OTP, salt, (err, hash) => {
                        if (err) throw err;
                        user.passwordOTP = hash;
                        user
                            .save()
                            .then((user) => {
                                var transporter = nodemailer.createTransport({
                                    service: "gmail",
                                    secure: true,
                                    port: 587,
                                    auth: {
                                        user: adminEmail,
                                        pass: adminPassword,
                                    },
                                    tls: {
                                        rejectUnauthorized: false,
                                    },
                                });

                                var mailOptions = {
                                    from: adminEmail,
                                    to: email.trim(),
                                    subject: "OTP Verification",
                                    text: `your OTP Verification Code is : ${OTP}`,
                                };
                                transporter.sendMail(mailOptions, (err, info) => {
                                    if (err) res.json({ message: "err", err });
                                    res.json({
                                        message: "email send successfully.",
                                        status: 1,
                                    });
                                });
                            })
                            .catch(() =>
                                res.json({ message: "Internal Server Error!", status: 0 })
                            );
                    });
                });
            })
            .catch(() =>
                res.json({ message: "Internal Server Error!", status: 0 })
            );
    }
};



/**
* @route Post /user/forgotPasswordOTPVerify
* @desc  user forgotPasswordOTPVerify Route
**/

exports.userForgotPasswordOTPVerifyRoute = (req, res, next) => {
    const { email, OTP } = req.body;

    if (!email) {
        return res.json({ message: "email must be required.", status: 0 });
    }

    if (!OTP) {
        return res.json({ message: "OTP must be required.", status: 0 });
    }

    if (email && OTP) {

        userSchema.findOne({ email })
            .then((user) => {
                if (!user) return res.json({ message: "email not found.", status: 0 });
                bcrypt.compare(OTP, user.passwordOTP).then((isMatch) => {
                    if (!isMatch)
                        return res
                            .json({ message: "OTP not match", status: 0 });
                    res.json({
                        message: "OTP match successfully.",
                        email: user.email,
                        status: 1,
                    });
                });
            })
            .catch(() =>
                res.json({ message: "Internal Server Error!", status: 0 })
            );
    }
};

/**
* @route Post /user/resetPassword
* @desc  user resetPassword Route
**/

exports.userResetPasswordRoute = (req, res, next) => {

    const { newPassword, confirmPassword, email } = req.body;

    if (!email) {
        return res.json({ message: "email must be required.", status: 0 });
    }

    if (!newPassword) {
        return res.json({ message: "Password must be required.", status: 0 });
    }

    if (!confirmPassword) {
        return res.json({ message: "confirm Password must be required.", status: 0 });
    }


    if (email && confirmPassword && newPassword) {

        userSchema.findOne({ email })
            .then((user) => {
                if (!user) return res.json({ message: "email not found.", status: 0 });
                if (newPassword !== confirmPassword)
                    return res.json({
                        message: "Your password and confirmation password do not match.",
                        status: 0,
                    });
                bcrypt.genSalt(10, (err, salt) => {
                    bcrypt.hash(newPassword, salt, (err, hash) => {
                        if (err) throw err;
                        user.password = hash;
                        user
                            .save()
                            .then((user) => {
                                res
                                    .json({
                                        message: "password change successfully",
                                        user,
                                        status: 1,
                                    });
                            })
                            .catch(() =>
                                res.json({ message: "Internal Server Error!", status: 0 })
                            );
                    });
                });
            })
            .catch(() =>
                res.json({ message: "Internal Server Error!", status: 0 })
            );
    }
};

/**
* @route Get /user/fetchUsers
* @desc  user fetchUsers Route
**/

exports.fetchUsersRoute = (req, res, next) => {
    userSchema.find()
        .then(users => {
            res.json({ message: "users Lists", status: 1, users })
        })
        .catch(() => res.json({ message: "internal server Error!", status: 0 }))
};

/**
* @route Get /user/findOneUser/:id
* @desc  user findOneUser Route
**/

exports.findOneUserRoute = (req, res, next) => {
    const id = req.params.id;
    if (!id) {
        return res.json({ message: "userID must be required.", status: 0 });
    }
    userSchema.findById(id)
        .then((id) => {
            if (!id) return res.json({ message: "User not found!", status: 0 });
            userSchema.findById(id)
                .then((user) => res.json({ message: "Find One User ", status: 1, user }))
                .catch(() => res.json({ message: "internal server Error!", status: 0 }))
        })
        .catch(() => res.json({ message: "internal server Error!", status: 0 }))
};

/**
* @route Get /user/addCoins
* @desc  user addCoins Route
**/

exports.addCoinsInUser = (req, res, next) => {
    const { coins, userID } = req.body;

    if (!userID) {
        return res.json({ message: "UserID must be required.", status: 0 });
    }
    userSchema.findById(userID)
        .then((user) => {
            if (!user) return res.json({ message: "UserID not found!", status: 0 });

            user.myEarnings += Number(coins);
            user.save()
                .then(() => {
                    res.json({ message: ` ${coins} Coins add successfully`, status: 1 })
                })
                .catch(() => res.json({ message: "internal server Error2!", status: 0 }))
        })
        .catch(() => res.json({ message: "internal server Error2!", status: 0 }))
}


/**
* @route Get /user/addRedeemInUser
* @desc  user addRedeemInUser Route
**/

exports.addRedeemInUser = (req, res, next) => {
    const { userID, SubCategoryID } = req.body;
    if (!userID) {
        return res.json({ message: "userID must be required.", status: 0 });
    }
    if (!SubCategoryID) {
        return res.json({ message: " SubCategoryID must be required.", status: 0 });
    }
    userSchema.findById(userID)
        .then((user) => {
            if (!user) return res.json({ message: "UserID not found!", status: 0 });
            StoreSubCategorySchema.findById(SubCategoryID)
                .then((subCategory) => {
                    if (!subCategory) return res.json({ message: "subCategory not found!", status: 0 });
                    if (user.myEarnings < subCategory.coins) return res.json({ message: "Your coins is not enough to get this Redeem", status: 0 });
                    user.myEarnings -= subCategory.coins;
                    const obj = {
                        subCategory: subCategory,
                        date: new Date().toDateString()
                    }
                    user.userRedeemHistory.push(obj)
                    user.save()
                        .then(() => {
                            res.json({ message: " Redeem add successfully.", status: 1 })
                        })
                        .catch(() => res.json({ message: "internal server Error!", status: 0 }))
                })
                .catch(() => res.json({ message: "internal server Error!", status: 0 }))
        })
        .catch(() => res.json({ message: "internal server Error!", status: 0 }))
}



