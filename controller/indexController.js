


/**
* @route Get /
* @desc  index Page Testing Route
**/

exports.indexTestingRoute = (req, res, next) => {
    res.json({ message: "index Testing Route" })
}

