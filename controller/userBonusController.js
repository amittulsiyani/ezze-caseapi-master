const userBonusSchema = require("../model/userBonusSchema");
const userSchema = require("../model/userSchema");
const BonusSchema = require("../model/BonusSchema");

var cron = require('node-cron');


/**
* @route Get /userBonus
* @desc  userBonus Page Testing Route
**/

exports.userBonusTestingRoute = (req, res, next) => {
    res.json({ message: " userBonus Testing Route" })
}

/**
* @route Post /userBonus/userBonusActive
* @desc  userBonusActive  Route
**/

/**
* @route Get Cron-JobApi
* @desc  Cron.Sehedule
**/

cron.schedule('* * * * * *', () => {
    userBonusSchema.find()
        .then(Bonuses => {
            const filterActiveBonuses = Bonuses.filter(data => {
                return (data.Active === true)
            })
            if (filterActiveBonuses.length > 0) {
                for (let i = 0; i < filterActiveBonuses.length; i++) {
                    const newTime = new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds() + ":" + new Date().toLocaleString('en-US', { hour: 'numeric', hour12: true })
                    const expireTime = filterActiveBonuses[i].expireTime.getHours() + ":" + filterActiveBonuses[i].expireTime.getMinutes() + ":" + filterActiveBonuses[i].expireTime.getSeconds() + ":" + filterActiveBonuses[i].expireTime.toLocaleString('en-US', { hour: 'numeric', hour12: true })
                    if (newTime === expireTime) {
                        filterActiveBonuses[i].Active = false
                        filterActiveBonuses[i].save()
                    }
                }
            }
        })
});

/**
* @route Post /userBonus/userBonusActive
* @desc  userBonusActive  Route
**/

exports.userBonusActiveRoute = (req, res, next) => {

    const { userID, BonusID, Active } = req.body;

    if (!userID) {
        return res.json({ message: "userID must be required.", status: 0 });
    }

    if (!BonusID) {
        return res.json({ message: "BonusID must be required.", status: 0 });
    }

    if (!Active) {
        return res.json({ message: "Active must be required.", status: 0 });
    }

    if (userID && BonusID) {
        var currentDate = new Date();
        var expireTime = new Date(currentDate.getTime() + 30 * 60000);
        const newUserBonuse = new userBonusSchema({ userID, BonusID, Active, expireTime })
        userSchema.findById(userID)
            .then((user) => {
                if (!user) return res.json({ message: "userID not found.", status: 0 });
                BonusSchema.findById(BonusID)
                    .then((Bonus) => {
                        if (!Bonus) return res.json({ message: "BonusID not found.", status: 0 });

                        userBonusSchema.find({ userID })
                            .then(Bonuses => {
                                const filterBonuses = Bonuses.filter(data => {
                                    return ((data.BonusID).toString() === BonusID)
                                })
                                console.log(filterBonuses)
                                if (filterBonuses.length > 0) {
                                    for (let i = 0; i < filterBonuses.length; i++) {

                                        if (filterBonuses[i].Active) return res.json({ message: "Bonus Already Active ", status: 0, expireTime });
                                        newUserBonuse.save()
                                            .then(() => {
                                                const obj = {
                                                    BounsInformation: Bonus,
                                                    active: Active,
                                                    date: new Date().toDateString()
                                                }
                                                user.userActivatedBonusesHistory.push(obj)
                                                user.save()
                                                    .then(() => {
                                                        res.json({ message: "Bonus Active Successfully.", status: 1 })
                                                    })
                                            })
                                            .catch(() => {
                                                res.json({ message: "Internal Server Error1!", status: 0 })

                                            })
                                    }
                                }

                                else {
                                    newUserBonuse.save()
                                        .then(() => {
                                            const obj = {
                                                BounsInformation: Bonus,
                                                active: Active,
                                                date: new Date().toDateString()
                                            }
                                            user.userActivatedBonusesHistory.push(obj)
                                            user.save()
                                                .then(() => {
                                                    res.json({ message: "Bonus Active Successfully.", status: 1 })
                                                })
                                        })
                                        .catch(() => {
                                            res.json({ message: "Internal Server Error2!", status: 0 })

                                        })
                                }
                            })
                            .catch(() =>
                                res.json({ message: "Internal Server Error3!", status: 0 })
                            );
                    })
                    .catch(() =>
                        res.json({ message: "Internal Server Error4!", status: 0 })
                    );
            })
            .catch(() =>
                res.json({ message: "Internal Server Error5!", status: 0 })
            );
    }
}

/**
* @route get /userBonus/fetchUsersActivatedBonuses
* @desc  fetchUserActivatedBonuses  Route
**/


exports.fetchUsersActivatedBonusesRoute = (req, res, next) => {
    userBonusSchema.find()
        .then(UsersActivatedBonuses => {
            res.json({ message: "Users Activated Bonuses ", status: 1, UsersActivatedBonuses })
        })
        .catch(() => res.json({ message: "internal server Error!", status: 0 }))
};

/**
* @route Get /userBonus/fetchUserActivatedBonusByID/:id
* @desc   fetchUserActivatedBonusByID  Route
**/


exports.fetchUserActivatedBonusByIDRoute = (req, res, next) => {
    const userID = req.params.id;


    if (!userID) {
        return res.json({ message: "userID must be required.", status: 0 });
    }

    userBonusSchema.find({ userID })
        .then(Bonuses => {
            res.json({ message: " Activated Bonuses Lists By CategoryId", status: 1, Bonuses })
        })
        .catch(() => res.json({ message: "internal server Error!", status: 0 }))
};

