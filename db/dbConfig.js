
var mongoose = require("mongoose");

var dataBaseURL = require("../keys/keys").url.mongodb;

mongoose.connect(dataBaseURL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false
})
    .then(() => { console.log("dataBase Connected") })
    .catch((err) => { console.log("dataBase not Connected ", err) })