var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require("cors");
const dotenv = require("dotenv")
dotenv.config()

var indexRouter = require('./routes/indexRoute');
var userRouter = require("./routes/userRoute");
var adminRouter = require("./routes/adminRoute");
var bonusRouter = require("./routes/BonusRoute");
var userBonusRouter = require("./routes/userBonusRoute");
var storeRouter = require("./routes/storeCategoryRoute");



var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

require("./db/dbConfig");

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.header('Access-Control-Allow-Headers', ' Origin , X-Requested-With , Content-Type , Accept , Authorization');
  res.header('Access-Control-Allow-Credentials', true);
  next();
});


app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use("/user", userRouter);
app.use("/admin", adminRouter);
app.use("/bonus", bonusRouter);
app.use("/userBonus", userBonusRouter);
app.use("/store", storeRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

app.listen(process.env.PORT, function (err) {
  if (err) console.log("Error in server setup")
  console.log("Server listening on Port", process.env.PORT);
})

module.exports = app;
